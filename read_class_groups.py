def read_json(infile):
    """read class groups from JSON file

    :param infile: input file (str)
    :return: class_groups (dict)
    """
    import json
    with open(infile, 'r') as f:
        class_groups = json.load(f)

    return class_groups

def read_class_groups():
    """
    return dictionary
    :return:
    """
    import csv
    with open('class_groups.csv', mode='r') as stuff:
        reader = csv.reader(stuff)
        mydict = {rows[0]:rows[1] for rows in reader}
    return mydict


def read_mat_choice(infile):
    """
    Auto figure out mat file, return dictionary of values
    :return:


    """
    import sys
    import h5py

    from scipy.io import loadmat


    try:
        d = loadmat(infile)
    except NotImplementedError:
        hfile = read_hdf5(infile)
        d = dict(hfile)
    return d

def read_csv(infile):
    """read class groups from CSV file

    :param infile: input file (str)
    :return: class_groups (dict)
    """
    import csv
    with open(infile, 'r') as f:
        c = csv.reader(infile)
        # FIX ME!!
        class_groups = ????

    return class_groups


def read_csv_numpy(infile):
    """read class groups from CSV file

    :param infile: input file (str)
    :return: class_groups (dict)
    """
    from numpy import loadtxt
    # COMPLETE ME!!

    return class_groups
